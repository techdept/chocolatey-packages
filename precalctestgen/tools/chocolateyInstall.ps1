﻿
$packageName = 'precalctestgen' # arbitrary name for the package, used in messages
$url = '\\nasapp\ChocoPack\Packages\Math\PreCalc\TestGen.zip' # download url
$url2 = '\\nasapp\ChocoPack\Packages\Math\PreCalc\TestGenShortcut.zip'
$url64 = $url # 64bit URL here or just use the same as $url
$url264 = $url2

  # Download zip files
  Get-ChocolateyWebFile "$packageName" 'C:\' "$url" "$url64"
  Get-ChocolateyWebFile "$packageName" 'C:\' "$url2" "$url264"
  # Unzip Files to correct location
  Get-ChocolateyUnzip "C:\TestGen.zip" "C:\Program Files\" #"$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
  Get-ChocolateyUnzip "C:\TestGenShortcut.zip" "C:\Documents and Settings\All Users\Start Menu\Programs\" #"$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

If (Test-Path "C:\Program Files\TestGen"){
    Remove-Item "C:\TestGen.zip"
}

If (Test-Path "C:\Documents and Settings\All Users\Start Menu\Programs\TestGen") {
    Remove-Item "C:\TestGenShortcut.zip"
}