﻿$packageName = 'calctestbank' # arbitrary name for the package, used in messages
$url = '\\nasapp\ChocoPack\Packages\Math\Calc\CALC3DF.zip' # download url
$url64 = $url # 64bit URL here or just use the same as $url

# Download Zip File
Get-ChocolateyWebFile "$packageName" 'C:\' "$url" "$url64"

# Unzip Test File
Get-ChocolateyUnzip "C:\CALC3DF.zip" "C:\Program Files\TestGen\Testbanks\" 

If (Test-Path "C:\Program Files\TestGen\Testbanks\CALC3DF.BOK"){
    Remove-Item "C:\CALC3DF.zip"
}