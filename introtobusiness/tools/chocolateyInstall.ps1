﻿#NOTE: Please remove any commented lines to tidy up prior to releasing the package, including this one

$packageName = 'introtobusiness' # arbitrary name for the package, used in messages
$installerType = 'EXE' #only one of these two: exe or msi
$url = '\\nasapp\ChocoPack\Packages\ExamView\Deca\introtobusiness\IntroToBusiness.zip' # download url
$url64 = $url # 64bit URL here or just use the same as $url
$silentArgs = '/S /v/qn' # "/s /S /q /Q /quiet /silent /SILENT /VERYSILENT" # try any of these to get the silent installer #msi is always /quiet
$validExitCodes = @(0) #please insert other valid exit codes here, exit codes for ms http://msdn.microsoft.com/en-us/library/aa368542(VS.85).aspx


  # other helpers - using any of these means you want to uncomment the error handling up top and at bottom.
  # downloader that the main helpers use to download items
  Get-ChocolateyWebFile "$packageName" 'C:\NACSManage\' "$url" "$url64"

  # unzips a file to the specified location - auto overwrites existing content
  Get-ChocolateyUnzip "C:\NACSManage\IntroToBusiness.zip" "C:\NACSManage\ITB\" 
  
  Install-ChocolateyInstallPackage "$packageName" "$installerType" "$silentArgs" 'C:\NACSManage\ITB\Setup.exe' -validExitCodes $validExitCodes
  
  
  If( Test-Path "C:\ExamView" ){
    Remove-Item "C:\NACSManage\ITB"
    Remove-Item "C:\NACSManage\IntroToBusiness.zip"
  }