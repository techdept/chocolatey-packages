﻿#NOTE: Please remove any commented lines to tidy up prior to releasing the package, including this one

$packageName = 'examview.mathCCGr2' # arbitrary name for the package, used in messages
$installerType = 'EXE' #only one of these two: exe or msi
$url = '\\nasapp\ChocoPack\Packages\ExamView\Gr2Math\Setup.exe' # download url
$url64 = $url # 64bit URL here or just use the same as $url
$silentArgs = '/S /v/qn'
$validExitCodes = @(0) 

Install-ChocolateyPackage "$packageName" "$installerType" "$silentArgs" "$url" "$url64"  -validExitCodes $validExitCodes