﻿
$packageName = 'photoshop7' # arbitrary name for the package, used in messages
$installerType = 'EXE' #only one of these two: exe or msi
$url = '\\nasapp\ChocoPack\Packages\Photoshop.zip' # download url
$url64 = $url # 64bit URL here or just use the same as $url
$silentArgs = '' # "/s /S /q /Q /quiet /silent /SILENT /VERYSILENT" # try any of these to get the silent installer #msi is always /quiet
$validExitCodes = @(0) #please insert other valid exit codes here, exit codes for ms http://msdn.microsoft.com/en-us/library/aa368542(VS.85).aspx


  # downloader that the main helpers use to download items
Get-ChocolateyWebFile "$packageName" 'C:\' "$url" "$url64"
Get-ChocolateyUnzip "C:\Photoshop.zip" "C:\"
  # installer, will assert administrative rights - used by Install-ChocolateyPackage
Install-ChocolateyInstallPackage "$packageName" "$installerType" "$silentArgs" 'C:\Photoshop\Setup.exe' -validExitCodes $validExitCodes
    # the following is all part of error handling


If (Test-Path "C:\Program Files\Adobe\Photoshop 7.0\Uninst.isu"){
    Remove-Item "C:\Photoshop.zip"
}

If (Test-Path "C:\Program Files\Adobe\Photoshop 7.0\Uninst.isu"){
    Remove-Item -Recurse -Force "C:\Photoshop"
}