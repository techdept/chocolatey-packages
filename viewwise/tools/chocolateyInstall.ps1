﻿
$packageName = 'viewwise' # arbitrary name for the package, used in messages
$installerType = 'EXE' #only one of these two: exe or msi
$url = '\\nasapp\ChocoPack\Packages\ViewWise\DT_Setup.exe' # download url
$zipUrl = '\\nasapp\ChocoPack\Packages\ViewWise\ViewWiseInstall.zip'
$url64 = $url # 64bit URL here or just use the same as $url
$silentArgs = '/s /f1C:\NACSManage\ViewWise\setup.iss' # "/s /S /q /Q /quiet /silent /SILENT /VERYSILENT" # try any of these to get the silent installer #msi is always /quiet
$validExitCodes = @(0) #please insert other valid exit codes here, exit codes for ms http://msdn.microsoft.com/en-us/library/aa368542(VS.85).aspx

# download and unpack a zip file that contains config files
Install-ChocolateyZipPackage "$packageName" "$zipUrl" "C:\NACSManage\ViewWise" 

# main helpers - these have error handling tucked into them already
# installer, will assert administrative rights
Install-ChocolateyPackage "$packageName" "$installerType" "$silentArgs" "$url" "$url64"  -validExitCodes $validExitCodes
# Create Desktop shortcut for Register file
Install-ChocolateyDesktopLink "C:\NACSManage\ViewWise\nacs.vws"
Install-ChocolateyDekstopLink "C:\Program Files\ViewWise Client\system\ViewWise.exe"